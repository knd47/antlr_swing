tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  //global scope = 1, rozmiar memory dla globalSymbols = 1
  Integer number = 1;
}
prog    : (e += range | e += expr | d += decl)* -> prog(name = {$e}, declaration = {$d});


range
      :
      ^(BEGIN_SCOPE {number = enterScope();} (e += range | e += expr | d += decl)* {number = leaveScope();}) -> var_scope(expression = {$e}, declaration = {$d})
      ;

decl  :
        ^(VAR i1=ID) {addNewVariable($ID.text, number);} -> dec(name = {$ID.text + number.toString()})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1 = {$e1.st}, p2 = {$e2.st})
        | ^(MINUS e1=expr e2=expr) -> sub(p1 = {$e1.st}, p2 = {$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mul(p1 = {$e1.st}, p2 = {$e2.st})
        | ^(DIV   e1=expr e2=expr) -> div(p1 = {$e1.st}, p2 = {$e2.st})
        | ^(PODST i1=ID   e2=expr) -> assign(name = {$i1.text + number.toString()}, value = {$e2.st})
        | ID                       -> id(name = {$ID.text + number.toString()})
        | INT                      -> int(i = {$INT.text})
    ;
    